package ru.sber.jd.mylib;

public class MyArrayList<T> {
    // Размер массива
    private int size = 0;
    // Шаг увеличения размера массива
    private int snap = 3;
    // Количество элементов выделенных для массива
    private int baseSize = 3;

    // массив
    private Object list[];

    public MyArrayList(){
        this.list = new Object[this.baseSize];
        this.size = 0;
    }

    //Добавить элемент в конец массива
    public void add(T object){
        this.resize();
        this.list[this.size] = object;
        this.size++;
    }

    //Добавить элемент в позицию индекс массива
    public void add(T object, int index){
        this.resize();
        for (int i = this.size; i > index; i--) {
            this.list[i] = this.list[i-1];
        }
        this.list[index] = object;
        this.size++;

    }

    //Удаление последнего элемента
    public void remove(){
        if(this.size > 0) {
            this.size--;
            this.resize();
        }
    }

    //Удаление элемент по индексу
    public void remove( int index){
        if(this.size > 0 && index < this.size) {
            for (int i = index; i < this.size-1; i++) {
                this.list[i] = this.list[i+1];
            }
            this.size--;
            this.resize();
        }
    }

    // Размер массива
    public int size(){
        return this.size;
    }

    // Вернуть элемент по индексу
    public Object get(int index){
        if(index > (this.size - 1) || index < 0 ){
            throw new ArrayIndexOutOfBoundsException("Выход за пределы MyArrayList в методе  get()");
        }
        return this.list[index];
    }

    /*
        Изменение количества элементов выделенных для массива
     */
    private void resize(){
        if( this.baseSize <= this.size + 1){
            this.baseSize += snap;
            Object[] list = new Object[baseSize];
            for (int i = 0; i < this.size; i++) {
                list[i] = this.list[i];
            }
            this.list = list;
        } else if(this.baseSize-snap > this.size){
            Object[] list = new Object[this.size];
            for (int i = 0; i < this.size ; i++) {
                list[i] = this.list[i];
            }
            this.list = list;
        }
    }


}
