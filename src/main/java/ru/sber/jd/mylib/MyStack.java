package ru.sber.jd.mylib;

public class MyStack<T extends Comparable> {

    private Entry last;    // текущий элемент стека

    public class Entry<T extends Comparable> {
        final Entry prev;
        final Entry min;
        T value;

        Entry(Entry prev, T value) {
            this.prev = prev;
            this.value = value;
            min = (prev == null || this.value.compareTo(prev.min.value) < 0) ? this : prev.min;
        }

        public T getValue() {
            return value;
        }

        public Object getMin() {
            return this.min.value;
        }
    }

    // создать стек 0 длины
    public MyStack() {
        this.last = null;
    }

    // создать стек c 1 элементом
    public MyStack(T value) {
        this.last = new Entry(null, value);
    }

    // создать стек и присвить значениями массива
    public MyStack( T arr[]) {
        for (T i : arr )
            this.last = new Entry(this.last, i);
    }

    // Добавить элемент в стек
    public void push( T value) {
        this.last = new Entry(this.last, value);
    }

    // Извлечь элемент из стека
    public T pop() {
        if (this.last == null) return null;
        T value = (T) this.last.getValue();
        this.last = this.last.prev;
        return value;
    }

    // Минимальный элемент
    public Object min() {
        return (this.last == null) ? null : this.last.getMin();
    }

}
