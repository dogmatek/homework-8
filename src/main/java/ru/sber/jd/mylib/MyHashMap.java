package ru.sber.jd.mylib;

public class MyHashMap<K, V> {
    // для лучшей повторной калибровки выбираем количесвто Bucket
    private static final int SIZE = 16;

    private Entry[] table= new Entry[SIZE];

    /**
     * Вложенный класс для хранения Map данных в паре ключ-значение.
     */
    public class Entry<K, V> {
        private final K key;
        private V value;
        Entry next;

        Entry(K k, V v) {
            key = k;
            value = v;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public K getKey() {
            return key;
        }
    }

    /**
     * Возвращает значение по ключу HashMap.
     */
    public Entry get(K k) {
        int hash = k.hashCode() % SIZE;
        //Решаем проблему с отрицательным Хэшем
        hash = (hash > 0) ? hash : -hash;

        Entry e = table[hash];

        // Bucket идентифицируется хэш-кодом и проходит по нему до тех пор, пока элемент не будет найден..
        while (e != null) {
            if (e.key.equals(k)) {
                return e;
            }
            e = e.next;
        }
        return null;
    }

    /**
     * Добавление нового ключ-значение
     * Если имеется сопоставление ключа, то старое значение заменяется новым.
     */
    public void add(K k, V v) {

        int hash = k.hashCode() % SIZE;

        //Решаем проблему с отрицательным Хэшем
        hash = (hash >= 0) ? hash : -hash;

        // System.out.println( k + "(" + k.hashCode() +"): " + hash);
        // Выбираем бакет
        Entry e = table[hash];
        if (e != null) {
            // Если найден дублика ключа
            if (e.key.equals(k)) {
                e.value = v;
            } else {
                // Вставляем новый элемент в конец списка bucket
                while (e.next != null) {
                    e = e.next;
                }
                Entry entryInOldBucket = new Entry(k, v);
                e.next = entryInOldBucket;
            }
        } else {
            // Создаём новый bucket для элемента.
            Entry entryInNewBucket = new Entry(k, v);
            table[hash] = entryInNewBucket;
        }
    }

    /*
       Поиск ключа по значению
     */
    public K contain(V v) {
        for (Entry e : table) {
            while (e != null) {
                if (e.value.equals(v)) {
                    return (K) e.key;
                }
                e = e.next;
            }
        }
        return null;
    }
}