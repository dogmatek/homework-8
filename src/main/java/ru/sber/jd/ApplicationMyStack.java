package ru.sber.jd;

import ru.sber.jd.mylib.MyStack;

public class ApplicationMyStack {
    public static void main(String[] args) {
       MyStack<Integer> stack = new MyStack( new Integer[]{10, 4, 20, 30, 1, 32, 21, 99});

        stack.push(2);
        stack.push(20);
        stack.push(17);
        stack.push(10);
        stack.push(3);
        stack.push(30);
        stack.push(15);
        stack.push(17);
        stack.push(77);
        stack.pop();
        stack.pop();

        System.out.println("Минимальный элемент: " + stack.min());
    }
}
