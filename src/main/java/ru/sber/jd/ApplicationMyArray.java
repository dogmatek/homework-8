package ru.sber.jd;

import ru.sber.jd.mylib.MyArrayList;

import java.util.HashMap;

public class ApplicationMyArray {
    public static void main(String[] args) {

        MyArrayList<Integer> myArrayList = new MyArrayList<>();
        myArrayList.add(10);
        myArrayList.add(11);
        myArrayList.add(12);
        myArrayList.add(13);
        myArrayList.add(14);
        myArrayList.add(15);
        myArrayList.add(22,2);
        myArrayList.add(25,6);

        myArrayList.remove(0);
        myArrayList.remove(2);
        myArrayList.remove();
        myArrayList.remove();
        myArrayList.remove();

        System.out.println("Размер массива: " + myArrayList.size());

        for (int i = 0; i < myArrayList.size(); i++) {
            System.out.println(myArrayList.get(i));
        }
    }
}
