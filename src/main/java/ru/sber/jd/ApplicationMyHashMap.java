package ru.sber.jd;

import ru.sber.jd.mylib.MyHashMap;

public class ApplicationMyHashMap {
    public static void main(String[] args) {
        MyHashMap<String,String> myHashMap = new MyHashMap();

        myHashMap.add("повар", "Иван");
        myHashMap.add("машинист", "Фёдор");
        //myHashMap.add("певец", "Глеб");
        myHashMap.add("строитель", "Семён");

        System.out.println(myHashMap.get("строитель").getKey());
        System.out.println(myHashMap.get("строитель").getValue());

       // myHashMap.add("строитель", "Дмитрий");
        myHashMap.add("строитель", "Пётр");
        myHashMap.add("учитель", "Сплинтер");

        System.out.println(myHashMap.get("строитель").getKey());
        System.out.println(myHashMap.get("строитель").getValue());

        System.out.println(myHashMap.contain("Иван"));
    }

}
